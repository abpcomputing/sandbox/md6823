# %%
import os
import glob
from pathlib import Path

# %%
# mkdir ADTObsBox_data
# sshfs nfs:/nfs/cfc-sr4-adtobs2buf/obsbox/slow ADTObsBox_data
files = glob.glob('ADTObsBox_data/disk*/'
                  '1/1[4]/B*_Q*_14h05*.h5')
len(files)
# %%
len_files =  len(files)
for n,file in enumerate(files):
    print(file)
    print(f'{n+1}/{len_files}')
    my_file = Path(file)
    local_folder = Path('/eos/project/a/abpdata/lhc/ObsBox/MD6823/data', 
                        *my_file.parts[1:-1])
    #local_folder = Path('local_copy', *my_file.parts[1:-1])

    local_folder.mkdir(parents=True, exist_ok=True)
    os.system(f'rsync --ignore-existing -raz --progress  {file} {local_folder/my_file.name}')


# %%

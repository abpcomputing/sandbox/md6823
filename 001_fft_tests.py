# %% Generic import
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

params = {'legend.fontsize': 18,
          'figure.figsize': (8., 5.),
          'axes.labelsize':  16,
          'axes.titlesize':  16,
          'xtick.labelsize': 15,
          'ytick.labelsize': 15,
          'image.cmap':'jet',
          'lines.markersize': 2 }
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rcParams.update(params)

get_ipython().magic('matplotlib inline')
%config InlineBackend.figure_format = 'retina' # retina display

# %% Define an harmonic oscillation for 3 bunches

f_sampling_Hz=11245.5
t_sampling_s=1/f_sampling_Hz
DeltaT_s=t_sampling_s/3
print(DeltaT_s)
n=np.arange(0,4096*4.) # turns

f_Hz_case1=2000.
f_Hz_case2=6000.
f_Hz_case3=14000.

x_bunch = {}
for i in range(3):
    x_bunch[i] = []
    for j in range(1):
        x_bunch[i].append( 5.0*(np.cos(2*np.pi*(f_Hz_case1)*(t_sampling_s*n + i*DeltaT_s + j*25e-9))) + 
                          2.5*(np.cos(2*np.pi*(f_Hz_case2)*(t_sampling_s*n + i*DeltaT_s + j*25e-9))) + 
                          1.0*(np.cos(2*np.pi*(f_Hz_case3)*(t_sampling_s*n + i*DeltaT_s + j*25e-9)))
                         )


# %% Computing the fft of each bunch
fourier_tot = []
fourier_tot_corr = []

f=np.linspace(0,f_sampling_Hz, len(n))
for i in range(3):
    fourier_tot.append(np.fft.fft(x_bunch[i][0])/len(np.abs(np.fft.fft(x_bunch[0][0])))*2.0)
    delay = i*DeltaT_s
    fourier_tot_corr.append(fourier_tot[-1] * np.exp(-1j*2*np.pi*f*delay))
                            
df = pd.DataFrame({"fourier": fourier_tot, "corrected_fourier":fourier_tot_corr})
df
# %% bbb FFT without considering the delay.

f=np.linspace(0,f_sampling_Hz, len(n))
fig, ax = plt.subplots(figsize=(12,5), ncols=3, sharex=True, sharey=True)

plt.sca(ax[0])
plt.title('1st bunch, 1st train')
plt.semilogy(f,abs(df.fourier.iloc[0]), c='k')
plt.ylabel('|FFT| [arb. units]')
plt.xlabel('f [Hz]')

plt.sca(ax[1])
plt.title('1st bunch, 2nd train')
plt.xlabel('f [Hz]')
plt.semilogy(f,abs(df.fourier.iloc[1]), c='r')

plt.sca(ax[2])
plt.title('1st bunch, 3rd train')
plt.xlabel('f [Hz]')
plt.semilogy(f,abs(df.fourier.iloc[2]), c='g')
plt.xlim(0,5600)
plt.ylim(1e-5, 1.5e0)
plt.tight_layout()
plt.axvline(f_Hz_case1)
plt.axvline(11245.5-f_Hz_case2)
plt.axvline(11245.5-f_Hz_case3)
# %%
df.fourier.iloc[0].real
idx = np.argmax(abs(df.fourier.iloc[0]))
idx

# %% Check correction

import matplotlib.pyplot as plt
idx = np.argmax(abs(df.fourier.iloc[0]))
##################### Corrected phase

fig, ax = plt.subplots(figsize=(8,6), ncols=2, sharex=True, sharey=True)


plt.sca(ax[0])
plt.plot([0.,df.fourier.iloc[0][idx].real], [0., df.fourier.iloc[0][idx].imag], marker='o',c='k')
plt.plot([0.,df.fourier.iloc[1][idx].real], [0., df.fourier.iloc[1][idx].imag], marker='o',c='b')
plt.plot([0.,df.fourier.iloc[2][idx].real], [0., df.fourier.iloc[2][idx].imag], marker='o',c='r')


plt.sca(ax[1])

plt.plot([0.,df.corrected_fourier.iloc[0][idx].real], [0., df.corrected_fourier.iloc[0][idx].imag], marker='o',c='k')
plt.plot([0.,df.corrected_fourier.iloc[1][idx].real], [0., df.corrected_fourier.iloc[1][idx].imag], marker='o',c='b')
plt.plot([0.,df.corrected_fourier.iloc[2][idx].real], [0., df.corrected_fourier.iloc[2][idx].imag], marker='o',c='r')


plt.sca(ax[0])
plt.ylabel(r'$\rm Re(FFT_{2 kHz}) $')
plt.xlabel(r'$\rm Im(FFT_{2 kHz}) $')
plt.axvline(0, c='k', lw=0.5)
plt.axhline(0, c='k', lw=0.5)
plt.sca(ax[1])
plt.xlabel(r'$\rm Im(FFT_{2 kHz}) $')

plt.axvline(0, c='k', lw=0.5)
plt.axhline(0, c='k', lw=0.5)


# %% Summing with the correct phasing

fig, ax = plt.subplots()
f=np.linspace(0,f_sampling_Hz, len(n))
aux = df.corrected_fourier.sum()/len(df)
plt.plot(f,np.abs(aux), c='b')
plt.ylabel('|FFT| [arb. units]')
plt.xlabel('f [Hz]')
plt.title('First bunch of each train')
#plt.ylim(1e-5, 1.5e0)
#plt.axvline(0.5*11245.5)

# %% The trivial approach: it works only for evenly spaced bunch 
# (during MD is OK but not regular fills)
aux=[]
for ii in range(len(n)):
    aux+=[x_bunch[0][0][ii],x_bunch[1][0][ii],x_bunch[2][0][ii]]

f=np.linspace(0,3*f_sampling_Hz, len(n)*3)
plt.plot(f, abs(np.fft.fft(aux))/len(np.abs(np.fft.fft(aux)))*2.0,'b')
plt.xlim(0,15000)
plt.ylabel('|FFT| [arb. units]')
plt.xlabel('f [Hz]')
plt.title('Summing (interleaving) in time-domain')
plt.xlim(0,15000)

# %% The same behavior can be found back by extending the fft domain by periodicity
# In this example we extend it by a factor 2 (until 2*f_sampling_Hz)
fourier_tot = []
fourier_tot_corr = []

f=np.linspace(0,f_sampling_Hz*3, len(n)*3)
for i in range(3):
    data= np.fft.fft(x_bunch[i][0])/len(np.abs(np.fft.fft(x_bunch[0][0])))*2.0
    fourier_tot.append(np.concatenate([data,data, data]))
    delay = i*DeltaT_s
    fourier_tot_corr.append(fourier_tot[-1] * np.exp(-1j*2*np.pi*f*delay))
                            
df = pd.DataFrame({"fourier": fourier_tot, "corrected_fourier":fourier_tot_corr})

fig, ax = plt.subplots()

aux = df.corrected_fourier.sum()/len(df)
plt.plot(f,np.abs(aux), c='b')
plt.ylabel('|FFT| [arb. units]')
plt.xlabel('f [Hz]')
plt.title('Summing (dephasing) in frequency-domain')
plt.xlim(0,15000)

# %% Half machine
n_bunches = 2000
filling_pattern = np.random.choice(np.arange(3564), n_bunches, replace=False)

f_sampling_Hz=11245.5
t_sampling_s=1/f_sampling_Hz
DeltaT_s = 25e-9
print(DeltaT_s)
n=np.arange(0,4096*4.) # turns

f_Hz_case1=2000.
f_Hz_case2=6000.
f_Hz_case3=14000.

x_bunch = {}
for i in range(n_bunches):
    x_bunch[i] = []
    for j in range(1):
        x_bunch[i].append( 5.0*(np.cos(2*np.pi*(f_Hz_case1)*(t_sampling_s*n + filling_pattern[i]*DeltaT_s + j*25e-9))) + 
                          2.5*(np.cos(2*np.pi*(f_Hz_case2)*(t_sampling_s*n + filling_pattern[i]*DeltaT_s + j*25e-9))) + 
                          1.0*(np.cos(2*np.pi*(f_Hz_case3)*(t_sampling_s*n + filling_pattern[i]*DeltaT_s + j*25e-9)))
                         )
fourier_tot = []
fourier_tot_corr = []

f=np.linspace(0,f_sampling_Hz*2, len(n)*2)
for i in range(n_bunches):
    data= np.fft.fft(x_bunch[i][0])/len(np.abs(np.fft.fft(x_bunch[0][0])))*2.0
    fourier_tot.append(np.concatenate([data,data]))
    delay = filling_pattern[i]*DeltaT_s
    fourier_tot_corr.append(fourier_tot[-1] * np.exp(-1j*2*np.pi*f*delay))
                            
df = pd.DataFrame({"fourier": fourier_tot, "corrected_fourier":fourier_tot_corr})

fig, ax = plt.subplots()

aux = df.corrected_fourier.sum()/len(df)
plt.plot(f,np.abs(aux), c='b')
plt.ylabel('|FFT| [arb. units]')
plt.xlabel('f [Hz]')
plt.title('Summing (dephasing) in frequency-domain')
plt.xlim(0,15000)
# %%

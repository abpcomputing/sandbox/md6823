# go to the folder you want to use

# from http://bewww.cern.ch/ap/acc-py/installers/                               
wget http://bewww.cern.ch/ap/acc-py/installers/acc-py-2020.11-installer.sh

bash ./acc-py-2020.11-installer.sh -p ./acc-py/base/2020.11/ -b

# activate some acc-py python distribution:
source ./acc-py/base/2020.11/setup.sh
   
# create your own virtual environment in the folder "py_tn":
acc-py venv py_test
   
# activate your new environment
source ./py_test/bin/activate
   
# and finish it with nxcals
python -m pip install jupyterlab matplotlib pandas pyarrow h5py
python -m pip install nxcals


# to get the kerberos token
# kdestroy && kinit -f -r 5d -kt your_keytab.keytab  your_username
# from https://nxcals-docs.web.cern.ch/current/user-guide/data-access/configuration/#kerberos-keytab-file-generation

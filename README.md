# MD6823


The procedure of the MD is on https://codimd.web.cern.ch/BqUvMki_T3mpzEEHPXXCmw

and on https://asm.cern.ch/md-planning/lhc-requests/6823/procedure

The report is on https://www.overleaf.com/read/jcmdsjtjsnhn

## Getting started

### Git cloning the repository
Connect to you preferred linux machine (we assume that has an internet access and `sshd`).

Go to your preferred folder and 

`git clone ssh://git@gitlab.cern.ch:7999/abpcomputing/sandbox/md6823.git`.  You need to have a SSH-key public key for that machine 
(from gitlab.cern.ch follow the `User Settings > SSh Keys` and https://gitlab.cern.ch/help/user/ssh.md  (that is type `ssh-keygen -t ed25519` to get you public key and copy and paste it to the `User Settings > SSh Keys` corresponding filed)).

### Editing `~/.ssh/config`

Starting from the `config` example, modify accordingly your  `~/.ssh/config` (mutatis mutandis).

### sshfs mounting
Map the ADTObsBox on your local folder by 
```
mkdir ADTObsBox_data 
sshfs nfs:/nfs/cfc-sr4-adtobs2buf/obsbox/slow ADTObsBox_data
```

### 50 Hz

You can find the RTE info at
https://www.services-rte.com/en/download-data-published-by-rte.html?category=public_transmission_system&type=network_frequencies
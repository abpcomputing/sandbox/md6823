# example of copying files from the beam test to eos 
# source /afs/cern.ch/user/s/skostogl/miniconda3/bin/activate
# sshfs nfs:/nfs/cfc-sr4-adtobs2buf/obsbox/slow ADTObsBox_data

import os
import numpy as np
import time
import glob
import subprocess
from pathlib import Path

path = 'ADTObsBox_data/'
# path = '/nfs/cfc-sr4-adtobs2buf/obsbox/slow/'
nfs_ssh = 'nfs'
destination_path = '/eos/project/a/abpdata/lhc/ObsBox/MD6823/data'
#my_dates = ['19', '20', '21', '22', '23']
my_dates=['29']
my_times = [10] #set to None if all times or e.g ['22'] for 22h
sleep_time = 120.0

while True:
    disks = subprocess.check_output(["ssh", "nfs", "ls", path]).decode("utf-8").split() 
    for disk in disks[:]:   
        dates = subprocess.check_output(["ssh", "nfs", "ls", f'{path}/{disk}']).decode("utf-8").split() 
        if my_dates:
            dates = [i for i in dates if i in my_dates]
            
        for date in dates:
            filenames_full = subprocess.check_output(["ssh", "nfs", "ls", f'{path}/{disk}/{date}/*/*.h5']).decode("utf-8").split() 
            times = [i.split('/')[-2] for i in filenames_full]
            # start of beam test, 19/10/2021 after 9 am
            if date == '19':
                times = [i for i in times if int(i)>9]
                filenames_full = [i for i in filenames_full if int(i.split('/')[-2])>9]
            ##
            if my_times:
                times = [i for i in times if i in my_times]
                filenames_full = [i for i in filenames_full if i.split('/')[-2] in my_times]
          
            # only Q8 pickup 
            filenames_full = np.array([i for i in filenames_full if '_Q8_' in i])
            filenames_full_split = np.array([i.split('/') for i in filenames_full])    
            [os.makedirs(f"{destination_path}/{i[-4]}/{i[-3]}/{i[-2]}", exist_ok=True) for i in filenames_full_split]

            
             
            destination_folders=np.array([f"{destination_path}/{i[-4]}/{i[-3]}/{i[-2]}/{i[-1]}" for i in filenames_full_split])
           
            if True: 
              # avoid copying existing files
              idx = [i for i in range(len(destination_folders)) if not os.path.exists(destination_folders[i])]
              if len(idx)>0:
                filenames_full = filenames_full[idx]
                filenames_full_split = filenames_full_split[idx]
                destination_folders = destination_folders[idx]
                print("######################################")
                print("######################################")
                print(f"Copying files from {path}/{disk}/{date} to {destination_path}/{disk}/{date}")
                print('')
                [subprocess.call(["rsync", "--ignore-existing", "-raz","--progress", f"{nfs_ssh}:{x}", y]) for x,y in zip(filenames_full, destination_folders)]
            else: 
              # copy everything and let rsync take care of it
              print("######################################")
              print("######################################")
              print(f"Copying files from {path}/{disk}/{date} to {destination_path}/{disk}/{date}")
              print('')
              [subprocess.call(["rsync", "--ignore-existing", "-raz","--progress", f"{nfs_ssh}:{x}", y]) for x,y in zip(filenames_full, destination_folders)]
    print(f'---------> Sleeping for {sleep_time} s.')
    time.sleep(sleep_time)

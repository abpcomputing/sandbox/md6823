# %%
import pandas as  pd
import h5py

# %%
#my_file =  '/home/sterbini/2022_06_24_MD6823/B2H_Q10_20220630_14h50m58s956251us.h5'
my_file = '/afs/cern.ch/user/s/sterbini/B2H_Q9_20220707_21h23m28s81666us.h5'
#my_file = '/eos/project/a/abpdata/lhc/ObsBox/data_13-14Jul/disk0/13/20/B2H_Q8_20220713_20h41m43s909038us.h5'
f = h5py.File(my_file, 'r')
# %%
list(f.keys())

# %%
list(f['B2'].keys())
#%%
list(f['B2']['attributes'])
# %%
assert f['B2']['attributes']['TurnCounterOK'][:]
assert all(f['B2']['attributes']['TurnCountersOK'][:])

# %%

my_DF = pd.DataFrame(f['B2']['horizontal'][:])
my_DF['TurnCountersOK'] = f['B2']['attributes']['TurnCountersOK'][:]
my_DF['TurnCounters'] = f['B2']['attributes']['TurnCounters'][:]
my_DF.columns = my_DF.columns.map(str)

# %%
my_DF.to_parquet('test.parquet')
# %%
df = my_DF.copy()
df = df.loc[:, (df != 0).any(axis=0)]
df.to_parquet('test_drop.parquet')

# %%

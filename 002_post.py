#%%
import h5py
import numpy as np
import datetime
import pandas as pd
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
# %%
dff = pd.read_parquet("FFT_test.parquet")
# %%
frev = 11245.5
# %% Spectrograms
x_lims = mdates.date2num(dff.time.values)  
freqs = np.linspace(0, frev*len(dff["bunches"].iloc[0]), len(dff["fourier_real"].iloc[0]))
myfilter = (freqs>0) & (freqs<frev*3*0.5)
fourier_abs = dff.apply(lambda x: abs(x.fourier_real + 1j*x.fourier_imag), axis=1).to_list()
#fig, ax = plt.subplots(figsize=(20,22))
fig, ax = plt.subplots()
plt.imshow(np.array(np.log10(fourier_abs)[:, myfilter]), aspect='auto', cmap='jet', extent=[freqs[myfilter][0], freqs[myfilter][-1], x_lims[0], x_lims[-1]])
plt.xlim(freqs[myfilter][0], freqs[myfilter][-1])
#plt.colorbar()
ax.yaxis_date()
date_format = mdates.DateFormatter('%H:%M:%S')
ax.yaxis.set_major_formatter(date_format)
#plt.title(f'{beam_plane}, {day}/10/2021, bunch {bunch}')
# This simply sets the x-axis data to diagonal so it fits better.
fig.autofmt_xdate()
plt.xlabel('f (Hz)')
fig.tight_layout()

# %% Simple FFT
dff["fourier_abs"] = dff.apply(lambda x: abs(x["fourier_real"] + 1j*x["fourier_imag"]), axis=1)
frev = 11245.5
freqs = np.linspace(0, frev*len(dff["bunches"].iloc[0]), len(dff["fourier_abs"].iloc[0]))
for i in range(len(dff)):
    plt.plot(freqs, dff["fourier_abs"].iloc[i])
plt.xlim(0, frev*len(dff["bunches"].iloc[0])*0.5)
plt.ylim(0,5.0)

# %%

import json
import yaml
import time
import pandas as pd
import tree_maker

import h5py
import numpy as np
import datetime
import pandas as pd

# Modified from https://github.com/sterbini/cl2pd/blob/master/cl2pd/noise.py

class ADT:
    """
    ===EXAMPLE===
    adt = ADT(filenames)
    myDict = adt.importEmptyDF
    myDF = myDict['B1H'].copy()
    myDF['Data'] = myDF['Path'].apply(adt.loadData)
    """
    
    def __init__(self,filenames):   
        myDATA      = {}
        beam_planes = []
        pus         = []
        times       = []   
        for fileName in filenames:
            current_fileName = fileName.split('/')[-1]
            beam_plane = current_fileName.split('_')[0]
            pu = current_fileName.split('_')[1]
            time = self.fromName2Timestamp(current_fileName)
            beam_planes.append(beam_plane)
            times.append(time)
            pus.append(pu)
        myfulldf = pd.DataFrame({'PU': pus, 'Beam-Plane': beam_planes, 'Path': filenames}, index=np.array(times))
        myfulldf.sort_index(inplace=True)
        for beam_plane in np.unique(beam_planes):
            myDATA[beam_plane] = myfulldf[myfulldf['Beam-Plane'] == beam_plane]
        self.importEmptyDF = myDATA
            
        
    def fromName2Timestamp(self, current_fileName,tz_start='CET',tz_end='UTC'):
        #print(current_fileName)
        year = current_fileName.split('_')[-2][0:4]
        month = current_fileName.split('_')[-2][4:6]
        day   = current_fileName.split('_')[-2][6:]
        full_datetime = f'{day}/{month}/{year} ' + current_fileName.split('_')[-1].split('.')[0]
        return pd.Timestamp(datetime.datetime.strptime(full_datetime,"%d/%m/%Y %Hh%Mm%Ss%fus")).tz_localize(tz_start, ambiguous=False).tz_convert(tz_end)


    def loadData(self, fileName):
            fi = h5py.File(fileName, 'r')
            beam  = (fileName.split('_')[-4].split('/')[-1])[0:2]
            plane = (fileName.split('_')[-4].split('/')[-1])[2:3]
            if plane == 'H':
              plane = 'horizontal'
            else:
              plane = 'vertical'   
            alldat = fi[beam][plane]
            print('Buffer: Turns = %s, Bunches = %s' %(alldat.shape[0], alldat.shape[1]))
            return alldat[:]    

    def cmp_fft(self, data, specific_bunch=None, average_fft=False, frev=11245.5, 
                correct_FFT=False, first_bunch_slot=0, bunch_spacing=25e-9, sampling_rate=1,
                periodic_filling_scheme=False):
        if specific_bunch is not None:
            fourier =  np.fft.fft(data[:,specific_bunch] )
            #print(specific_bunch, data[:,specific_bunch].shape)
            fourier =fourier/len(fourier)*2.0 
            freqs = np.linspace(0, frev, len(fourier))
            # see example 001_fft_tests.py from Guido
            if correct_FFT:
                #print("Correct FFT flag enabled")
                #print(f"Current bunch {specific_bunch}", f" First bunch {first_bunch_slot}", data[:,specific_bunch].shape)
                delay   = (specific_bunch-first_bunch_slot)*bunch_spacing
                fourier = np.concatenate([fourier]*sampling_rate)
                freqs   = np.linspace(0, frev*sampling_rate, len(fourier))
                corrected_fourier = fourier*np.exp(-1j*2.0*np.pi*freqs*delay)
                return pd.Series([freqs, fourier.real, fourier.imag, corrected_fourier.real, corrected_fourier.imag])
            else:
                return pd.Series([freqs, fourier.real, fourier.imag])
        elif average_fft:
            # find bunch slots
            idx = np.where(abs(data[0,:])>0.0)[0]
            fourier = np.fft.fft(data[:,idx], axis=0)
            fourier =fourier/len(fourier)*2.0
            avg_fourier = np.average(abs(fourier), axis=1)
            freqs = np.linspace(0, frev, len(fourier))
            return pd.Series([freqs, avg_fourier])
        elif periodic_filling_scheme:
            # append bbb data for each turn, only for periodic filling schemes
            idx = np.where(abs(data[0,:])>0.0)[0]
            all_dat = data[:,idx].flatten()
            fourier = np.fft.fft(all_dat)
            fourier =fourier/len(fourier)*2.0
            freqs = np.linspace(0, len(idx)*frev, len(fourier))
            return pd.Series([freqs, fourier])




with open('config.yaml','r') as fid:
    config=yaml.safe_load(fid)

tree_maker.tag_json.tag_it(config['log_file'], 'started')

adt_df=pd.read_parquet(config['adt_file'])

filenames_to_consider = adt_df['filename'].values[:]

# Approach with time domain concat for periodic filling scheme
adt = ADT(filenames_to_consider)
myDict = adt.importEmptyDF

fourier_tot_real = []
fourier_tot_imag = []
bunch_number_tot = []
time_tot         = []
pu_tot           = []
beam_plane_tot   = []
idx_tot          = []

for beamplane in myDict.keys():
    print(beamplane)
    myDF = myDict[beamplane].copy()
    try:
      myDF['Data'] = myDF['Path'].apply(adt.loadData)
      
      for counter in range(len(myDF)):
          data = myDF.iloc[counter]['Data']
          time = myDF.iloc[counter].name
          pu   = myDF.iloc[counter].PU
          current_beamplane = myDF.iloc[counter]['Beam-Plane']
          idx = np.where(abs(data[0,:])>0.0)[0]
          try:
          	freqs, fourier = adt.cmp_fft(data,periodic_filling_scheme=True)
          	fourier_tot_real.append(fourier.real)
          	fourier_tot_imag.append(fourier.imag)      
          	time_tot.append(time)
          	pu_tot.append(pu)
          	idx_tot.append(idx)
          	beam_plane_tot.append(current_beamplane)
          except:
            print("prob, fft")          
    except:
        print('prob, load_data')
dff = pd.DataFrame({'fourier_real': fourier_tot_real, 'fourier_imag': fourier_tot_imag,  "bunches": idx_tot, 'time': time_tot, 'beam-plane': beam_plane_tot, 'pu': pu_tot}) 
destination = config['save_to']
#dff.to_pickle(f"{destination}.pickle")
dff.to_parquet(f"{destination}.parquet")

tree_maker.tag_json.tag_it(config['log_file'], 'completed')

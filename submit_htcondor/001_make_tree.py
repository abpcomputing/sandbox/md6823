# %%
import tree_maker
from tree_maker import NodeJob
import time
import os
import itertools
import numpy as np
import pandas as pd
import glob

for day in [24]:
  path = '/eos/project-a/abpdata/lhc/ObsBox/MD6823'
  save_to = '/eos/project-a/abpdata/lhc/ObsBox/fft_analysis_MD6823/'
  filenames = (glob.glob(f"{path}/*/{day}/*/B*_Q*"))
  n = 20
  
  # Split in chunks
  filenames_in_chunks = [filenames[i * n:(i + 1) * n] for i in range((len(filenames) + n - 1) // n )] 
  
  #### The root of the tree
  start_time = time.time()
  
  # %%
  #root
  my_folder = os.getcwd()
  root = NodeJob(name='root', parent=None)
  root.path = my_folder + f'/test_new_{day}'
  root.template_path = my_folder + '/templates'
  root.log_file = root.path + "/log.json"
  
  
  distributions_folder = root.template_path + '/adt_paths'
  os.makedirs(distributions_folder, exist_ok=True)
  
  for ii,my_list in enumerate(filenames_in_chunks):
      pd.DataFrame(my_list, columns=['filename']).to_parquet(f'{distributions_folder}/{day}_{ii:03}.parquet')
  
  
  for node in root.root.generation(0):
      node.children=[NodeJob(name=f"{day}_{child:03}",
                             parent=node,
                             path=f"{node.path}/{day}_{child:03}",
                             template_path = root.template_path+'/000_analyse',
                             submit_command = f'condor_submit {root.template_path}/000_analyse/run.sub',
                             log_file=f"{node.path}/{day}_{child:03}/log.json",
                             dictionary={'adt_file':f'{distributions_folder}/{day}_{child:03}.parquet',
                                         'save_to': f'{save_to}/fft_{day}_{child:03}'
                                        })
                     for child, adt_file in enumerate(filenames_in_chunks)]
  
  root.to_json()
  
  print('Done with the tree creation.')
  print("--- %s seconds ---" % (time.time() - start_time))
  
  
  # %%
  """
  ### Cloning the templates of the nodes
  From python objects we move the nodes to the file-system.
  """
  
  # %%
  # We map the pythonic tree in a >folder< tree
  start_time = time.time()
  root.clean_log()
  root.rm_children_folders()
  from joblib import Parallel, delayed
  
  from shutil import copytree, copy
  import shutil
  def clone_children(self):
          for child in self.children:
              os.makedirs(child.path, exist_ok=True)
              #copy(child.template_path+'/config.yaml', child.path+'/config.yaml')
              #copy(child.template_path+'/*', child.path+'/')
              shutil.copytree(child.template_path, child.path, dirs_exist_ok=True)
              child.to_json()
  
  
  NodeJob.clone_children=clone_children
  
  for depth in range(root.height):
      [x.clone_children() for x in root.generation(depth)]
      # Parallel(n_jobs=8)(delayed(x.clone_children)() for x in root.generation(depth))
  
  # VERY IMPORTANT, tagging
  root.tag_as('cloned')
  print('The tree structure is moved to the file system.')
  print("--- %s seconds ---" % (time.time() - start_time))

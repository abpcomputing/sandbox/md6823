#%%
import h5py
import numpy as np
import datetime
import pandas as pd
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
# %%
class ADT:
    """
    ===EXAMPLE===
    adt = ADT(filenames)
    myDict = adt.importEmptyDF
    myDF = myDict['B1H'].copy()
    myDF['Data'] = myDF['Path'].apply(adt.loadData)
    """
    
    def __init__(self,filenames):   
        myDATA      = {}
        beam_planes = []
        pus         = []
        times       = []   
        for fileName in filenames:
            current_fileName = fileName.split('/')[-1]
            beam_plane = current_fileName.split('_')[0]
            pu = current_fileName.split('_')[1]
            time = self.fromName2Timestamp(current_fileName)
            beam_planes.append(beam_plane)
            times.append(time)
            pus.append(pu)
        myfulldf = pd.DataFrame({'PU': pus, 'Beam-Plane': beam_planes, 'Path': filenames}, index=np.array(times))
        myfulldf.sort_index(inplace=True)
        for beam_plane in np.unique(beam_planes):
            myDATA[beam_plane] = myfulldf[myfulldf['Beam-Plane'] == beam_plane]
        self.importEmptyDF = myDATA
            
        
    def fromName2Timestamp(self, current_fileName,tz_start='CET',tz_end='UTC'):
        #print(current_fileName)
        year = current_fileName.split('_')[-2][0:4]
        month = current_fileName.split('_')[-2][4:6]
        day   = current_fileName.split('_')[-2][6:]
        full_datetime = f'{day}/{month}/{year} ' + current_fileName.split('_')[-1].split('.')[0]
        return pd.Timestamp(datetime.datetime.strptime(full_datetime,"%d/%m/%Y %Hh%Mm%Ss%fus")).tz_localize(tz_start, ambiguous=False).tz_convert(tz_end)


    def loadData(self, fileName):
            fi = h5py.File(fileName, 'r')
            beam  = (fileName.split('_')[-4].split('/')[-1])[0:2]
            plane = (fileName.split('_')[-4].split('/')[-1])[2:3]
            if plane == 'H':
              plane = 'horizontal'
            else:
              plane = 'vertical'   
            alldat = fi[beam][plane]
            print('Buffer: Turns = %s, Bunches = %s' %(alldat.shape[0], alldat.shape[1]))
            return alldat[:]    

    def cmp_fft(self, data, specific_bunch=None, average_fft=False, frev=11245.5, 
                correct_FFT=False, first_bunch_slot=0, bunch_spacing=25e-9, sampling_rate=1,
                periodic_filling_scheme=False):
        if specific_bunch is not None:
            fourier =  np.fft.fft(data[:,specific_bunch] )
            #print(specific_bunch, data[:,specific_bunch].shape)
            fourier =fourier/len(fourier)*2.0 
            freqs = np.linspace(0, frev, len(fourier))
            # see example 001_fft_tests.py from Guido
            if correct_FFT:
                #print("Correct FFT flag enabled")
                #print(f"Current bunch {specific_bunch}", f" First bunch {first_bunch_slot}", data[:,specific_bunch].shape)
                delay   = (specific_bunch-first_bunch_slot)*bunch_spacing
                fourier = np.concatenate([fourier]*sampling_rate)
                freqs   = np.linspace(0, frev*sampling_rate, len(fourier))
                corrected_fourier = fourier*np.exp(-1j*2.0*np.pi*freqs*delay)
                return pd.Series([freqs, fourier.real, fourier.imag, corrected_fourier.real, corrected_fourier.imag])
            else:
                return pd.Series([freqs, fourier.real, fourier.imag])
        elif average_fft:
            # find bunch slots
            idx = np.where(abs(data[0,:])>0.0)[0]
            fourier = np.fft.fft(data[:,idx], axis=0)
            fourier =fourier/len(fourier)*2.0
            avg_fourier = np.average(abs(fourier), axis=1)
            freqs = np.linspace(0, frev, len(fourier))
            return pd.Series([freqs, avg_fourier])
        elif periodic_filling_scheme:
            # append bbb data for each turn, only for periodic filling schemes
            idx = np.where(abs(data[0,:])>0.0)[0]
            all_dat = data[:,idx].flatten()
            fourier = np.fft.fft(all_dat)
            fourier =fourier/len(fourier)*2.0
            freqs = np.linspace(0, len(idx)*frev, len(fourier))
            return pd.Series([freqs, fourier])

# Also needed outside of class for fast sorting of filenames
def fromName2Timestamp(current_fileName,tz_start='CET',tz_end='UTC'):
        #print(current_fileName)
        year = current_fileName.split('_')[-2][0:4]
        month = current_fileName.split('_')[-2][4:6]
        day   = current_fileName.split('_')[-2][6:]
        full_datetime = f'{day}/{month}/{year} ' + current_fileName.split('_')[-1].split('.')[0]
        return pd.Timestamp(datetime.datetime.strptime(full_datetime,"%d/%m/%Y %Hh%Mm%Ss%fus")).tz_localize(tz_start, ambiguous=False).tz_convert(tz_end)
# %%
# Find all files for specific date and time and sort them by time
import glob
path = 'ADTObsBox_data/'

# disk10/26/23/B1H_Q8_20211026_23h05m00s80808us.h5
# disk/day/time/filename
filenames = (glob.glob(f"{path}/*/18/6/B1H_Q8*"))
filenames.sort(key = lambda x: fromName2Timestamp(x.split("/")[-1]))
filenames
# %%
np.diff([fromName2Timestamp(x.split("/")[-1]) for x in filenames])
# %%
# Filter filenames by date, this is a fast way to filter
filenames_to_consider = filenames

t1 = pd.Timestamp('2022-06-18 04:00:00.635911+0000',tz="UTC")
t2 = pd.Timestamp('2022-06-18 04:00:09.635911+0000', tz="UTC")

t1 = t1.tz_convert("CET")
t2 = t2.tz_convert("CET")
print(f"Selected files from {t1} to {t2} CET")
print("")

filenames_to_consider = [i for i in filenames_to_consider if (fromName2Timestamp(i.split("/")[-1])>=t1) and (fromName2Timestamp(i.split("/")[-1])<=t2)]
print(filenames_to_consider)


#%%
# Create fake data compatible with ADTobsbox h5 files
nn=4096*4#65536
fake_data = np.zeros((nn, 3564), dtype='float')

f_sampling_Hz=11245.5
t_sampling_s=1/f_sampling_Hz
DeltaT_s=t_sampling_s/3
n=np.arange(0,nn)

f_Hz_case1=2000.
f_Hz_case2=6000.
f_Hz_case3=14000.

i=0
fake_data[:, 0] = 5.0*(np.cos(2*np.pi*(f_Hz_case1)*(t_sampling_s*n + i*DeltaT_s )))


idx = [0, 1188, 1188*2]
for i in range(len(idx)):
    fake_data[:, idx[i]] = ( 5.0*(np.cos(2*np.pi*(f_Hz_case1)*(t_sampling_s*n + i*DeltaT_s ))) + 
                          2.5*(np.cos(2*np.pi*(f_Hz_case2)*(t_sampling_s*n + i*DeltaT_s ))) + 
                          1.0*(np.cos(2*np.pi*(f_Hz_case3)*(t_sampling_s*n + i*DeltaT_s )))
                    )

with h5py.File('B1H_Q8_20250618_04h00m05s26426us.h5','w') as f:
    group = f.create_group('B1')
    group.create_dataset(name='horizontal', data=fake_data)

with h5py.File('B1H_Q8_20250618_04h01m05s26426us.h5','w') as f:
    group = f.create_group('B1')
    group.create_dataset(name='horizontal', data=fake_data)

filenames_to_consider = ["B1H_Q8_20250618_04h00m05s26426us.h5", "B1H_Q8_20250618_04h01m05s26426us.h5"]


# %%
# Approach with time domain concat for periodic filling scheme
adt = ADT(filenames_to_consider)
myDict = adt.importEmptyDF

fourier_tot_real = []
fourier_tot_imag = []
time_tot         = []
pu_tot           = []
beam_plane_tot   = []
idx_tot          = []
for beamplane in myDict.keys():
    print(beamplane)
    myDF = myDict[beamplane].copy()
    #try:
    if True:
      myDF['Data'] = myDF['Path'].apply(adt.loadData)
      
      for counter in range(len(myDF)):
          data = myDF.iloc[counter]['Data']
          time = myDF.iloc[counter].name
          pu   = myDF.iloc[counter].PU
          idx = np.where(abs(data[0,:])>0.0)[0]
          current_beamplane = myDF.iloc[counter]['Beam-Plane']
          freqs, fourier = adt.cmp_fft(data,periodic_filling_scheme=True)
          fourier_tot_real.append(fourier.real)
          fourier_tot_imag.append(fourier.imag)      
          time_tot.append(time)
          pu_tot.append(pu)
          idx_tot.append(idx)
          beam_plane_tot.append(current_beamplane)
          

dff = pd.DataFrame({'fourier_real': fourier_tot_real, 'fourier_imag': fourier_tot_imag,  "bunches": idx_tot, 'time': time_tot, 'beam-plane': beam_plane_tot, 'pu': pu_tot}) 
dff.to_parquet("FFT_test.parquet")
# %%
dff["fourier_abs"] = dff.apply(lambda x: abs(x["fourier_real"] + 1j*x["fourier_imag"]), axis=1)
frev = 11245.5
freqs = np.linspace(0, frev*len(dff["bunches"].iloc[0]), len(dff["fourier_abs"].iloc[0]))
for i in range(len(dff)):
    plt.plot(freqs, dff["fourier_abs"].iloc[i])
plt.xlim(0, frev*len(dff["bunches"].iloc[0])*0.5)
plt.ylim(0,5.0)


# %%
# Approach with dephasing
adt = ADT(filenames_to_consider)
myDict = adt.importEmptyDF

fourier_tot_real = []
fourier_tot_imag = []
fourier_tot_real_corr  = []
fourier_tot_imag_corr  = []
bunch_number_tot = []
time_tot         = []
#freqs_tot        = []
pu_tot           = []
beam_plane_tot   = []


sampling_rate=3

for beamplane in myDict.keys():
    print(beamplane)
    myDF = myDict[beamplane].copy()
    #try:
    if True:
      myDF['Data'] = myDF['Path'].apply(adt.loadData)
      
      for counter in range(len(myDF)):
          data = myDF.iloc[counter]['Data']
          time = myDF.iloc[counter].name
          pu   = myDF.iloc[counter].PU
          current_beamplane = myDF.iloc[counter]['Beam-Plane']
          # for each bunch
          idx = np.where(abs(data[0,:])>0.0)[0]
          print(f"Bunch slots {idx}")
          for idd in idx:
              #try:
              if True:
                #freqs, fourier_real, fourier_imag = adt.cmp_fft(data, specific_bunch=idd)
                
                freqs, fourier_real, fourier_imag, fourier_corr_real, fourier_corr_imag = adt.cmp_fft(data, specific_bunch=idd,
                                                                correct_FFT=True, first_bunch_slot=idx[0], bunch_spacing=25e-9, sampling_rate=sampling_rate)
                fourier_tot_real.append(fourier_real)
                fourier_tot_imag.append(fourier_imag)
                fourier_tot_real_corr.append(fourier_corr_real)
                fourier_tot_imag_corr.append(fourier_corr_imag)
                
                bunch_number_tot.append(idd)
                time_tot.append(time)
                #freqs_tot.append(freqs)
                pu_tot.append(pu)
                beam_plane_tot.append(current_beamplane) 
              #except:
              #  print('prob') 
    #except:
    #    print('prob, load_data')
#dff = pd.DataFrame({'fourier_real': fourier_tot_real, 'fourier_imag': fourier_tot_imag,  'bunch': bunch_number_tot, 'time': time_tot, 'beam-plane': beam_plane_tot, 'pu': pu_tot})
dff = pd.DataFrame({'fourier_corr_real': fourier_tot_real_corr, "fourier_corr_imag": fourier_tot_imag_corr,'fourier_real': fourier_tot_real, 'fourier_imag': fourier_tot_imag,  'bunch': bunch_number_tot, 'time': time_tot, 'beam-plane': beam_plane_tot, 'pu': pu_tot})


# %%

for key, group in dff.groupby("time"):
    print(len(group))
    group2 = group.copy()
    aux = group2.apply(lambda x: x.fourier_corr_real + 1j*x.fourier_corr_imag, axis=1).sum()
    aux = aux/len(group2)
    print(aux.shape)
    
    freqs = np.linspace(0, 11245.5*sampling_rate, len(aux))
    ##df = group[group["bunch"] == bunch_number]
    #print(df)
    ##freqs = np.linspace(0,11245.5, len(df.fourier_real.values[0]))
    ##fourier_abs = df.apply(lambda x: abs(x.fourier_real + 1j*x.fourier_imag), axis=1).values[0]
    #print(fourier_abs)
    fig, ax = plt.subplots()
    plt.plot(freqs, abs(aux)) 
    plt.xlim(0,11245.5)
    plt.xlabel("f (Hz)")
    plt.ylabel(r"FFT ($\rm \mu m$)")
    plt.title(key)
    plt.ylim(0,5.0)
    plt.xlim(0, frev*sampling_rate*0.5)
    
    #for i in range(0, 12000, 50):
    #    plt.axvline(i, linewidth=0.3, c='dimgray')
# %%
